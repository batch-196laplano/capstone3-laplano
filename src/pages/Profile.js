import {useState, useEffect, useContext} from 'react';
import {Container, Card} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Profile(){
const {user, setUser} = useContext(UserContext);

	const retrieveUserDetails = (token) =>{
		fetch('https://limitless-retreat-83617.herokuapp.com/users/getUserDetails', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data) //accessToken
	
			console.log(user)//userDetailss
		})
	};

	return(
	<Container>
		<Card.Title>
		<h1>User Profile</h1>
		</Card.Title>
			<Card.Text>First Name: {user.firstname} </Card.Text>
			<Card.Text>Last Name: {user.lastname}	</Card.Text>
			<Card.Text>Mobile Number: {user.mobileNo}	</Card.Text>
			<Card.Text>Email: {user.email}	</Card.Text>
			{
				(user.isAdmin !== true)?
				<Card.Text>Account Type: Regular User</Card.Text>
				:
				<Card.Text>Account Type: Admin</Card.Text>
			}

	</Container>
	) 

}