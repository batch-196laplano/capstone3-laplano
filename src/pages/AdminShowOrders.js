import { useEffect, useState, useContext } from "react";
import { Button, Table } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import AllOrderCard from '../components/AllOrderCard';
import UserContext from '../UserContext';

export default function UserOrder() {

    const {user, setUser} = useContext(UserContext);
    const redirect = useNavigate();
    const [orders, setOrders] = useState([]); 
    
    useEffect(() => {
        fetch("https://limitless-retreat-83617.herokuapp.com/users/orders", {
             headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then((data) => {
          console.log(data);

            setOrders(data.map(orderList => {
                return (
                    <AllOrderCard key={orderList._id} orderProp={orderList} />
                )
            }))
        })
    }, []);

    return (
        (user.isAdmin) ?
<>
  <h1>ADMIN DASHBOARD</h1>
  <Button style={{margin:5}}variant="primary" as={Link} to="/AdminProducts">Manage Products</Button>

            <Table bordered className="mt-5">
                <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Total Amount</th>
                        <th>Date Purchased</th>
                        <th>Order Id</th>
                    </tr>
                </thead>
                <tbody>
                    { orders }
                </tbody>
            </Table>
        </>
        :
        <Navigate to="/products"/>
    );
}


