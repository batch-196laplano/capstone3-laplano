import {useState, useEffect} from 'react';
import ProductCard from '../components/ProductCard'

export default function Products() {

	const [products, setProducts] = useState([]);
//backend
	useEffect(()=>{
		fetch("https://limitless-retreat-83617.herokuapp.com/products/products")
		.then(res => res.json())
		.then(data => {
			// console.log(data); //products

			setProducts(data.map(product => {
				return(
						<ProductCard key={product._id} productProp = {product}/>
					)
			}));
		});
	},[]);


	return(
	<>
		<h1>Available Products:</h1>
		{products}
	</>
	)
}

