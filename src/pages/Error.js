import {Link} from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';

export default function Error(){
  return(
    <Row>
      <Col className ="p-5">
        <h1>404 - Not Found</h1>
        <p>The Page you are looking cannot be found.</p>
        <Button as={Link} to="/" variant="primary">Back Home</Button>
      </Col>
    </Row>
  )
}