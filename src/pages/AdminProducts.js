import {useState, useEffect,useContext} from 'react';
import {Container, Col, Row, Table, Button, Modal, Form} from 'react-bootstrap';
import AdminProductCard from '../components/AdminProductCard';
import {useParams, useNavigate, Link} from 'react-router-dom';
//import AdminAddProduct from '../components/AdminAddProduct';
import Products from '../pages/Products';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminProductView(){

	const {user} = useContext(UserContext);

	// const {breakpoint, productProp} = props
	//const {name, description, price, _id} = productProp;

	const [products, setProducts] = useState([]);
	
	//const history = useNavigate();

	// useStates for Add
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isActiveBtn, setIsActiveBtn] = useState(false);

	const [showAdd, setShowAdd] = useState(false)
	const handleAddClose = () => setShowAdd(false);
	const handleAddShow = () => setShowAdd(true);

	const [addedProduct, setAddedProduct] = useState(false);
	const [dis, setDis] = useState(()=> setProducts)

	//Function for adding products
	function addNewProduct(){
		 // e.preventDefault();

		fetch('https://limitless-retreat-83617.herokuapp.com/products/addProduct',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
		 				name: name,
		 				description: description,
		 				price: price
			 	})
		 	})	
		 	.then(res => res.json())
		 	.then(data => {
		 		console.log(data)
		 			Swal.fire({
		 				title: 'New Product Added Successfully!',
		 				icon: 'success',
		 				text: 'Add more products'
		 				});
		 	// setAddedProduct(true);
			setShowAdd(false);
			setName('');
			setDescription('');
			setPrice('');

		 	})
		}
	
	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if(name !== '' && description !== '' && price!== '' && price !== 0){
			setIsActiveBtn(true);

		} else {
			setIsActiveBtn(false);
		}

	}, [name, description, price]);


	useEffect(()=> {
		fetch("https://limitless-retreat-83617.herokuapp.com/products/getAllProducts",{
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			const productsArr = (data.map(product => {
				return(
					<AdminProductCard key={product._id} productProp = {product}/>
				)
			}))
			setProducts(productsArr)
		});
	},[name, description, price]);

	return (
		(user.id === null)?	
		<Products/>
		:
		<>
  	<h1>ADMIN DASHBOARD</h1>
  	<Button style={{margin:5}} variant="success" as={Link} to="/AdminShowOrders">Show User Orders</Button>
		<th colSpan={2}  className="text-center"><Button style={{margin:5}} variant="primary" onClick={handleAddShow}>Add Product</Button></th>
		<Table striped hover>
      		<thead>
      		<tr>
      		<th colSpan={12} className="text-center mb-0"><h3 className="mb-0">PRODUCT LIST</h3></th>
      	  </tr>
      		<tr>
			       <th>NAME</th>
			       <th>DESCRIPTION</th>
			       <th>PRICE</th>
			       <th>STATUS</th>
			       <th>DELETE</th>
			       <th>UPDATE</th>
			       <th>DISABLE</th>
		        </tr>
      		</thead>
      		{/*{ 
      			addedProduct !== true ?
      			<tbody>	
      				{products}
      			</tbody>*/}
      		{/*	:
      			<>*/}
      			<tbody>
      				{products}
      			</tbody>
      		{/*	</>
      		}*/}
    	</Table>
    	

      <Modal show={showAdd} onHide={handleAddClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={e => addNewProduct()}>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Here"
                value={name}
                autoFocus
                onChange={e => setName(e.target.value)}
              />
            </Form.Group>

            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Product Description</Form.Label>
              <Form.Control 
              as="textarea" 
              rows={3} 
              placeholder="Product Description Here"
              value={description}
              onChange={e => setDescription(e.target.value)}
              />
            </Form.Group>


            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Price"
                value={price}
                onChange={e => setPrice(e.target.value)}
                autoFocus
              />
            </Form.Group>


          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleAddClose}>
            Close
          </Button>
          { isActiveBtn ?
            <Button variant="primary" type="submit" id="submitBtn" onClick={(e) => addNewProduct()}>
              Save New Product
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
              Save New Product
            </Button>
          }

        </Modal.Footer>
      </Modal>
		</>
	)
}
