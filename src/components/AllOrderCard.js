import { useEffect, useState, useContext } from "react";

export default function AllOrderCard({ orderProp }) {

    const { firstname, lastname } = orderProp;
    console.log(orderProp);

 	useEffect(()=>{
 	})
    return (

        orderProp.orders.map((orderList) =>
            orderList.products.map((productList) =>
               
                <tr key={productList._id}>
                    <td>{firstname} {lastname}</td>              
                    <td>{ productList.productName }</td>
                    <td>{ productList.quantity }</td>
                    <td>{ orderList.totalAmount }</td>
                    <td>{ orderList.purchasedOn }</td>
                    <td>{ orderList._id }</td>
                </tr>
                
            )
        )
    );
}
