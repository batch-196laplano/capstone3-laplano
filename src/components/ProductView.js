import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){
	const {user} = useContext(UserContext)

	const history = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const {productId} = useParams();

	const createOrder = (productId) =>{
		fetch('https://limitless-retreat-83617.herokuapp.com/users/createOrder',{
			method : 'POST',
			headers:{
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then (res => res.json())
		.then (data=> {
			// console.log(data)

			if(user.isAdmin!==true){
				Swal.fire({
					title: "Added to cart",
					icon:'success',
					text: 'Thank You'
				});
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon:'error',
					text: 'Please try again later'
				});
			};
		});
	};

	useEffect(()=>{
		console.log(productId)
		fetch (`https://limitless-retreat-83617.herokuapp.com/products/getSingleProduct/${productId}`)
		.then (res => res.json())
		.then (data =>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});

	},[productId]);

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							{user.id !==null?
								<Button variant="primary" onClick={()=> createOrder(productId)}>Buy</Button>
								:
								<Link className="btn btn-danger" to="/login">Log In</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
