import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard(props){
	// console.log(props);
	// console.log(typeof props);

	//object destructuring
	const {name, description, price, _id ,isActive} = props.productProp

	return(
		<Card className="cardHighlight p-3">
			<Card.Body>
				<Card.Title className="fw-bold">{name}</Card.Title>

				<Card.Subtitle>Product Description:</Card.Subtitle> 
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn-primary" to={`/ProductView/${_id}`}>View Details</Link>

{/*				<Card.Subtitle>Availability:</Card.Subtitle>
				<Card.Text>{isActive.toString()}</Card.Text>*/}

			</Card.Body>
		</Card>
	)
};

