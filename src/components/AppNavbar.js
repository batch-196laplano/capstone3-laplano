import {useContext} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container ,Nav} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar(){

  const {user} = useContext(UserContext);

  console.log(user);

  return (
  <Navbar bg="light" expand="lg">
    <Container>
      <Navbar.Brand as={Link} to="/">Ecommerce App</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
        
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/products">Products</Nav.Link>

          {
            (user.id !== null && user.isAdmin === true) ?
            <>
              <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
              <Nav.Link as={Link} to="/AdminShowOrders">Admin Dashboard</Nav.Link>
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            </>
            :(user.id !== null && user.isAdmin === false) ?
            <>
              <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
              <Nav.Link as={Link} to="/strechgoal" disabled>View Cart</Nav.Link>
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            </>
            :
            <>
              <Nav.Link as={Link} to="/login">Login</Nav.Link>
              <Nav.Link as={Link} to="/register">Register</Nav.Link>
            </>
          }


        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
  )
}