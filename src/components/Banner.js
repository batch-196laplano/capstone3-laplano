import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){
	return(
		<Row>
			<Col className ="p-5">
				<h1>Ecommerce App</h1>
				<p>Your One Stop Shop!</p>
				<Button variant="primary" as={Link} to="/login">Shop Now</Button>
			</Col>
		</Row>
	)
}