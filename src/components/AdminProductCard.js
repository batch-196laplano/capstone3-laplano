import {useState,useEffect, useContext} from 'react';
import {Table,  Button, Col, Modal, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductCard({productProp}){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const {name, description, price, _id, isActive} = productProp;
	//console.log(productProp._id);

	//Activate
	const [activeProduct, setActiveProduct] = useState(true)

	//delete
	const [showDelete, setShowDelete] = useState()
	const handleCloseDel = () => setShowDelete(false);
	const handleShowDel = () => setShowDelete(true);

	// const id = productProp.id;

	const [productEditName, setProductEditName] = useState(name);
	const [productEditDescription, setProductEditDescription] = useState(description);
	const [productEditPrice, setProductEditPrice] = useState(price);


	const [productName, setProductName] = useState(productEditName);
	const [productDescription, setProductDescription] = useState(productEditDescription);
	const [productPrice, setProductPrice] = useState(productEditPrice);
	const [productIsActive, setProductisActive] = useState(isActive);

	// for Edit 
	const [showEdit, setShowEdit] = useState(false);
	const handleCloseEdit = () => {
		setShowEdit(false);
		// setProductEditName(productName);
		// setProductEditDescription(productDescription);
		// setProductEditPrice(productPrice);
	}
	const handleShowEdit = () => setShowEdit(true);	
	const [deletedProduct, setDeletedProduct] = useState(false);

	let status = "Available";

	if (isActive){
		status ='Available';
	} else {
		status ='Not Available';
	}

	const [productStatus, setProductStatus] = useState(status);

	function pageReload() {
        window.location.reload(true);
  }

	function updateProduct(_id) {	
		fetch(`https://limitless-retreat-83617.herokuapp.com/products/updateProduct/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productEditName,
				description: productEditDescription,
				price: productEditPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
			setProductEditName(data.name);
			setProductEditDescription(data.description);
			setProductEditPrice(data.price);
			
			setProductName(productEditName);
			setProductDescription(productEditDescription);
			setProductPrice(productEditPrice);

			setShowEdit(false)
		})
	}

	//Activating

	function activateProduct(_id){

		fetch(`https://limitless-retreat-83617.herokuapp.com/products/activateProduct/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
			
		})
			setProductisActive(true);
			setProductStatus("On Sale")
	}

	//Deactivate
	function deactivateProduct(_id){

		fetch(`https://limitless-retreat-83617.herokuapp.com/products/archiveProduct/${_id}`,{
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
				setProductisActive(false);
				setProductStatus("Not On Sale")
		})
	}

//Delete
	function deleteProduct(_id){
		// console.log(_id)
			fetch(`https://limitless-retreat-83617.herokuapp.com/products/deleteProduct/${_id}`,{
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
		 			title: 'Deleted Product',
		 			icon: 'success',
		 			text: 'The product is deleted.'
		 		});
					setShowDelete(false);
					setDeletedProduct(true);
		})
		}

	useEffect(() => {
		
		
	},[])


	return (
		(deletedProduct !== true) ? 
		<>
		<tr variant="primary">
          <td>{productName}</td>
          <td>{productDescription}</td>
          <td>{productPrice}</td>
          <td>{productStatus}</td>
          <td><Button variant="secondary" disabled/*onClick={() => deleteProduct(_id)}*/>Delete</Button></td>
          <td><Button variant="success" onClick={handleShowEdit}>Update</Button></td>
          <td>
          { productIsActive === true ? 
             <Button variant="outline-warning" onClick={e => deactivateProduct(_id)}>Deactive</Button>
            :
             <Button variant="warning" onClick={e => activateProduct(_id)}>Activate</Button>
          }
          </td>
        </tr>

		<Modal show={showEdit} onHide={handleCloseEdit} >
			        <Modal.Header closeButton>
			        <Modal.Title>Update Product's Info</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form >
							<Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Name</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productEditName}
						        onChange = {e => setProductEditName(e.target.value)}
						        autoFocus
						    />
							</Form.Group>

						    <Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Description</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productEditDescription}
						        onChange = {e => setProductEditDescription(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="productPrice">
						    <Form.Label>Price</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productEditPrice}
						        onChange = {e => setProductEditPrice(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						</Form>
						</Modal.Body>
					<Modal.Footer>
					   
					    { (productEditName === productName && productEditDescription === productDescription && productEditPrice === productPrice ) ?
					    	<Button variant="danger" onClick={() => updateProduct(_id)} disabled>
					    					    Save Changes
					    	</Button>
					    					    :
					    	<Button variant="primary" onClick={() => updateProduct(_id)}>
					    					Save Changes
					    	</Button>
					    					  }
					</Modal.Footer>
			</Modal> 


			<Modal show={showDelete} onHide={handleCloseDel}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you will delete this product?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDel}>
            Close
          </Button>
          <Button variant="primary" onClick={() => deleteProduct(_id)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
			</>
			:
		 deleteProduct(_id)
	)
}
